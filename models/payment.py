# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import hashlib

from werkzeug import urls

from odoo import api, fields, models, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.tools.float_utils import float_compare

import logging

_logger = logging.getLogger(__name__)


class PaymentAcquirerKhalti(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[
        ('khalti', 'Khalti')
    ], ondelete={'khalti': 'set default'})
    # khalti_merchant_key = fields.Char(string='Merchant Key', required_if_provider='khalti', groups='base.group_user')
    # khalti_merchant_salt = fields.Char(string='Merchant Salt', required_if_provider='khalti', groups='base.group_user')
    khalti_public_key = fields.Char(string='Public Key', required_if_provider='khalti', groups='base.group_user')
    khalti_secret_key = fields.Char(string='Secret Key', required_if_provider='khalti', groups='base.group_user')

    def _get_khalti_urls(self, environment):
        """ Khalti URLs"""
        if environment == 'prod':
            return {'khalti_form_url': 'https://khalti.com/api/v2/payment/initiate/'}
            # return {'khalti_form_url': 'https://secure.payu.in/_payment'}
        else:
            # return {'khalti_form_url': 'https://sandboxsecure.payu.in/_payment'}
            return {'khalti_form_url': 'https://khalti.com/api/v2/payment/initiate/'}

    def _khalti_generate_sign(self, inout, values):
        """ Generate the shasign for incoming or outgoing communications.
        :param self: the self browse record. It should have a shakey in shakey out
        :param string inout: 'in' (odoo contacting khalti) or 'out' (khalti
                             contacting odoo).
        :param dict values: transaction values

        :return string: shasign
        """
        if inout not in ('in', 'out'):
            raise Exception("Type must be 'in' or 'out'")

        if inout == 'in':
            # keys = "key|txnid|amount|productinfo|firstname|email|udf1|||||||||".split('|')
            keys = "public_key|mobile|transaction_pin|amount|product_identity|product_name|||||||||".split('|')
            sign = ''.join('%s|' % (values.get(k) or '') for k in keys)
            # sign += self.khalti_merchant_salt or ''
            sign += self.khalti_secret_key or ''
        else:
            # keys = "|status||||||||||udf1|email|firstname|productinfo|amount|txnid".split('|')
            keys = "|status||||||||||product_name|product_identity|amount|transaction_pin|mobile".split('|')
            sign = ''.join('%s|' % (values.get(k) or '') for k in keys)
            # sign = self.khalti_merchant_salt + sign + self.khalti_merchant_key
            # below one line added
            sign = self.khalti_secret_key + sign + self.khalti_public_key

        shasign = hashlib.sha512(sign.encode('utf-8')).hexdigest()
        return shasign

    def khalti_form_generate_values(self, values):
        self.ensure_one()
        base_url = self.get_base_url()
        khalti_values = dict(values,
                                # key=self.khalti_merchant_key,
                                # txnid=values['reference'],
                                # amount=values['amount'],
                                # productinfo=values['reference'],
                                # firstname=values.get('partner_name'),
                                # email=values.get('partner_email'),
                                # phone=values.get('partner_phone'),
                                # service_provider='payu_paisa',

                                # Above made into these belows
                                public_key=self.Khalti_public_key,
                                mobile=values.get('partner_phone'),
                                amount=values['amount'],
                                product_identity=values['reference'],
                                product_name=values['reference'],
                                surl=urls.url_join(base_url, '/payment/khalti/return'),
                                furl=urls.url_join(base_url, '/payment/khalti/error'),
                                curl=urls.url_join(base_url, '/payment/khalti/cancel'),
                                service_provider='khalti_paisa'
                                )

        # khalti_values['udf1'] = khalti_values.pop('return_url', '/')
        khalti_values['hash'] = self._khalti_generate_sign('in', khalti_values)
        return khalti_values

    def khalti_get_form_action_url(self):
        self.ensure_one()
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_khalti_urls(environment)['khalti_form_url']


class PaymentTransactionKhalti(models.Model):
    _inherit = 'payment.transaction'

    @api.model
    def _khalti_form_get_tx_from_data(self, data):
        """ Given a data dict coming from khalti, verify it and find the related
        transaction record. """
        reference = data.get('txnid')
        pay_id = data.get('mihpayid')
        shasign = data.get('hash')
        if not reference or not pay_id or not shasign:
            raise ValidationError(_('Khalti: received data with missing reference (%s) or pay_id (%s) or shasign (%s)') % (reference, pay_id, shasign))

        transaction = self.search([('reference', '=', reference)])

        if not transaction:
            error_msg = (_('Khalti: received data for reference %s; no order found') % (reference))
            raise ValidationError(error_msg)
        elif len(transaction) > 1:
            error_msg = (_('Khalti: received data for reference %s; multiple orders found') % (reference))
            raise ValidationError(error_msg)

        #verify shasign
        shasign_check = transaction.acquirer_id._Khalti_generate_sign('out', data)
        if shasign_check.upper() != shasign.upper():
            raise ValidationError(_('Khalti: invalid shasign, received %s, computed %s, for data %s') % (shasign, shasign_check, data))
        return transaction

    def _khalti_form_get_invalid_parameters(self, data):
        invalid_parameters = []

        if self.acquirer_reference and data.get('mihpayid') != self.acquirer_reference:
            invalid_parameters.append(
                ('Transaction Id', data.get('mihpayid'), self.acquirer_reference))
        #check what is buyed
        if float_compare(float(data.get('amount', '0.0')), self.amount, 2) != 0:
            invalid_parameters.append(
                ('Amount', data.get('amount'), '%.2f' % self.amount))

        return invalid_parameters

    def _khalti_form_validate(self, data):
        status = data.get('status')
        result = self.write({
            'acquirer_reference': data.get('khaltiId'),
            'date': fields.Datetime.now(),
        })
        if status == 'success':
            self._set_transaction_done()
        elif status != 'pending':
            self._set_transaction_cancel()
        else:
            self._set_transaction_pending()
        return result
