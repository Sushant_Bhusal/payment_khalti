# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import pprint
import werkzeug

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class KhaltiController(http.Controller):
    @http.route(['/payment/khalti/return', '/payment/khalti/cancel', '/payment/khalti/error'], type='http', auth='public', csrf=False)
    def payu_return(self, **post):
        """ Khalti."""
        _logger.info(
            'Khalti: entering form_feedback with post data %s', pprint.pformat(post))
        if post:
            request.env['payment.transaction'].sudo().form_feedback(post, 'khalti')
        return werkzeug.utils.redirect('/payment/process')
